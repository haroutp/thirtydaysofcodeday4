using System;

namespace Day4
{
    class Person
    {
        private int age;

        public Person(int initialAge)
        {
            if(initialAge < 0){
                age = 0;
                System.Console.WriteLine("Age is not valid, setting age to 0.");
            }else{
                age = initialAge;
            }
        }

        public void amIOld(){
            // Do some computations in 
            //here and print out the correct statement to the console
            if(age < 13){
                System.Console.WriteLine("You are young.");
            }else if(age >= 13 && age < 18){
                System.Console.WriteLine("You are a teenager.");
            }else{
                System.Console.WriteLine("You are old.");
            }
        }

        public void yearPasses()
        {
            // Increment the age of the person in here
            age++;
        }

    }
}
